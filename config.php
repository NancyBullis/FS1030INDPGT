<?php
define('DB_SERVER', 'cloudsql');
define('DB_USERNAME', 'user');
define('DB_PASSWORD', 'password');
define('DB_NAME', 'db');

$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

if($link === false) {
die("ERROR: Could not connect. " . mysqli_connect_error());
}
?>